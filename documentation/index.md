# Initial User Story
As a client organization, we would like to whitelist geographic regions at the country level that our users are allowed to log in from so they are prevented from outsourcing their work
## Tasks
* <em>(DONE)</em> At the API gateway capture the IP and restrictions then if there are pass those pieces of data to another microservice that will return an object indicating if the IP originates from somewhere in the list
* <em>(InProgress)</em> Create a new micro service to check if a given IP is physically located in a given list of geographic regions
    * <em>(InProgress)</em> Create an ASP.Net Core MVC project for the service
        * <em>(InProgress)</em> Create a controller for whitelists
        * <em>(InProgress)</em> Add current version of MaxMind GeoLite2 country database
    * <em>(InProgress)</em> Create unit tests for the new service
        * <em>(TODO)</em> Create positive test cases
        * <em>(TODO)</em> Create negative test cases