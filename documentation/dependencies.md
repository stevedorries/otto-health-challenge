# Dependencies

## External Files
The GeoFilter service currently depends on the MaxMind GeoLite2 database, available [here](https://geolite.maxmind.com/download/geoip/database/GeoLite2-Country.tar.gz)

## External Packages
* MaxMind.Db - Used to open the GeoLite database files
* MaxMind.GeoIP2 - Convenience wrapper for the MaxMind.Db package
