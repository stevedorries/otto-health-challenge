# OTTO Health Challenge

Coding exercise for application for position at OTTO Health.

## Scenario 
A large prospective customer has requested the ability to restrict his users to logging in to their UI accounts from selected countries to prevent them from outsourcing their work to others. 

The product team has designed a solution where the customer database will hold the white listed countries and the API gateway will capture the requesting IP address, check the target customer for restrictions, and send the data elements to a new service you are going to create.  

The new service will be an HTTP-based API that receives an IP address and a white list of countries.  The API should return an indicator if the IP address is within the the listed countries. You can get a data set of IP address to country mappings from https://dev.maxmind.com/geoip/geoip2/geolite2/.
