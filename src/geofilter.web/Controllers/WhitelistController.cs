﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using MaxMind.GeoIP2;
using MaxMind.GeoIP2.Responses;
using Microsoft.AspNetCore.Mvc;

namespace Otto.GeoFilter.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WhitelistController : ControllerBase
    {
        // POST api/values/8.8.8.8
        /// <summary>
        /// Checks the geolocation database for the given IP Address, 
        /// then determines if the IP comes from one of the white listed
        /// values provided in the request body
        /// </summary>
        [HttpPost("{ip}")]        
        public ActionResult Post([FromRoute] string ip, [FromBody] string[] regions)
        {
            string countryOfOrigin = "";
            // Create a DB connection
            using (var reader = new DatabaseReader("GeoLite2-Country.mmdb"))
            {
                CountryResponse response;
                if( reader.TryCountry(ip, out response))
                    countryOfOrigin = response.Country.IsoCode;

            }
            return new JsonResult(new { HostOriginatedInList = regions.Contains(countryOfOrigin) });
        }
    }
}
