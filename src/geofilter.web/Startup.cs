﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Otto.GeoFilter
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            // services.AddSwaggerDocument();
            services.AddOpenApiDocument();
            // This would normally be a SQL server logging provider of some variety, but without one running it seems overkill to add such things
            services.AddLogging(  config => { config.AddConsole();}  );
            // Using a singleton will make requests more effecient, but requires some careful thought, since there is the possiblity of having an 
            // automated update mechanism for the DB file that this would connect to and be shared with all service requests 
            // such considerations would vary depending on the exact type of update mechanism that is used, I did not choose to 
            // implement one, as there are too many variables undefined to choose the most appropriate one. 
            // However I did want to note this as a point of consideration when designing the service.
            // var serv = services.AddSingleton(typeof(MaxMind.GeoIP2.IGeoIP2DatabaseReader), typeof(MaxMind.GeoIP2.DatabaseReader));
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();            
            app.UseMvc();
            app.UseSwagger(); // Serves the registered OpenAPI/Swagger documents by default on `/swagger/{documentName}/swagger.json`
            app.UseSwaggerUi3(); // Serves the Swagger UI 3 web ui to view the OpenAPI/Swagger documents by default on `/swagger`
        }
    }
}
