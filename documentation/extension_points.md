# Potential Enhancements
* Add caching
    - It would be possible to create a chaching strategy where the results of each POST operation are stored in a redis instance or something similar to it, the key could be a hash of the IP address and the given array of countries and the value just the boolean result of the lookup operation. Changes to the workflow to support the cache would be to attempt a GET on the whitelist like GET /whitelist/{hashOfIpAndCountryCodes}, if a 404 is returned then the system attempting to use the service would POST the hash with the IP and country code list in the body of the request like POST /whitelist/{hashGoesHere} "{"ip": "8.8.8.8", "regions": ["US","EU","IE"]}" subsequent attempts to GET the value out of cache would then work avoiding expensive queries to the DB
* Add region level granularity
    - It's foreseeable that another client may want to prevent people from logging in at a more specific level than nation-state, adding multiple levels of granularity would require a small amount of refactoring, enough to dissuade me from implementing this without a user initiating it, but not enough to make it painful to add it later if it becomes needed 
* Add blacklist endpoint
    - It's possible that a different client may in the future want to blacklist countries/regions instead of whitelisting, the amount of effort to implement it as a future addition would be minimal, but it would be helpful to have thought of the possibility just to remind ourselves not to make the service harder to extend than it has to be
# Automated Database Updates
From what I can see there are three viable methods to automatically update the IP database:
* Pay for the hosted version
    - This is the simplist solution and takes the update completely out of our scope
    - This costs money, analysis of traffic would need to be completed to determine if the benefits outweigh the costs
* Scheduled monthly events:
    - Download the file to a central location with a scheduled event on a server that is always online and another monthly event in the containers to copy the newer version when it is available
        + This has a higher complexity than just throwing money at the problem
        + This may pay off quickly enough to justify the additional complexity and development time needed to implement it
* Do it Application Code:
    - Before doing any of the DB queries and whitelist checks, check if our .mmdb file has the same MD5 as the current one
        + This is ugly
        + This produces a huge amount of network traffic for no good reason
        + This may lead to multiple versions of the file existing depending on the number of instances that are currently running which would make the results non-deterministic, which is not acceptable under most circumstances. 